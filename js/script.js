function validform() {

    var a = document.forms["my-form"]["full-name"].value;
    var b = document.forms["my-form"]["email-address"].value;
    var c = document.forms["my-form"]["username"].value;
    var d = document.forms["my-form"]["password"].value;
    var e = document.forms["my-form"]["repat_password"].value;
    if (d != e)
    {
        alert("Girmiş olduğunuz iki parola eşleşmiyor...!")
        return false;
    }
    if (a==null || a=="")
    {
        alert("Lütfen ad soyadınızı giriniz");
        return false;
    }else if (b==null || b=="")
    {
        alert("Lütfen e-mail adresinizi giriniz");
        return false;
    }else if (c==null || c=="")
    {
        alert("Lütfen kullanıcı adınızı giriniz");
        return false;
    }else if (d==null || d=="")
    {
        alert("Lütfen parola belirleyiniz");
        return false;
    }else if (e==null || e=="")
    {
        alert("Belirlediğiniz parolayı tekrar giriniz");
        return false;
    }

}
